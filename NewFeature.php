<?php

//enum merupakan feature baru pada php 8.1
enum Status {
    case Pending;
    case Success;
    case Failed;
    case Expired;
    
    public function getStatus(): string {
        return match($this){
            Status::Pending => 'pending',
            Status::Success => 'success',
            Status::Failed => 'failed',
            Status::Expired => 'expired',
        };
    }
}

class Transaction {
    public function __construct(public Status $status){}
    
    public function setStatus(Status $status) : void {
        $this->status = $status;
    }
}


$transaction = new Transaction(Status::Pending, 'Sleman');

//fiber digunaikan untuk memanage perintah secara pararel
$fiber = new Fiber(function (): void {
    $valueAfterResuming = Fiber::suspend('after suspending');
});

$valueAfterResuming = $fiber->start();
$fiber->resume('after resuming');